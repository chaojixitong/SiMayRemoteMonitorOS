﻿using SiMay.ReflectCache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SiMay.Core
{
    public class ScreenSetClipoardPacket : EntitySerializerBase
    {
        public string Text { get; set; }
    }
}
